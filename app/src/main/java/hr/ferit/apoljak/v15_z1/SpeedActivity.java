package hr.ferit.apoljak.v15_z1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class SpeedActivity extends Activity implements View.OnClickListener {

    Spinner spinnerSpeed1;
    Spinner spinnerSpeed2;
    ArrayAdapter<CharSequence> adapterSpeed1;
    ArrayAdapter<CharSequence> adapterSpeed2;
    EditText etEnterValue;
    Button bConvert;
    String speedFrom="";
    String speedTo="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed);

        etEnterValue = (EditText) findViewById(R.id.etEnterValue);
        bConvert = (Button) findViewById(R.id.bConvert);
        bConvert.setOnClickListener(this);
        spinnerSpeed1 = (Spinner) findViewById(R.id.spinnerSpeed1);
        spinnerSpeed2 = (Spinner) findViewById(R.id.spinnerSpeed2);
        adapterSpeed1 = ArrayAdapter.createFromResource(this, R.array.spinnerSpeedArray1, android.R.layout.simple_spinner_item);
        adapterSpeed2 = ArrayAdapter.createFromResource(this, R.array.spinnerSpeedArray2, android.R.layout.simple_spinner_item);
        adapterSpeed1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterSpeed2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSpeed1.setAdapter(adapterSpeed1);
        spinnerSpeed2.setAdapter(adapterSpeed2);

        spinnerSpeed1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                speedFrom = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSpeed2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view2, int position, long id) {
                speedTo = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public double KmhToMph()
    {
        double valueKtM = Double.parseDouble(etEnterValue.getText().toString());
        return valueKtM / 1.609;
    }

    public double MphToKmh()
    {
        double valueMtK = Double.parseDouble(etEnterValue.getText().toString());
        return valueMtK * 1.609;
    }

    @Override
    public void onClick(View v)
    {
        Intent intent = new Intent();
        intent.setClass(this, ResultActivity.class);

        String SpeedFrom = speedFrom;
        intent.putExtra(ResultActivity.KEY_CONVERTEDFROM, SpeedFrom);
        String SpeedTo = speedTo;
        intent.putExtra(ResultActivity.KEY_CONVERTEDTO, SpeedTo);
        double ValueFrom = Double.parseDouble(etEnterValue.getText().toString());
        intent.putExtra(ResultActivity.KEY_VALUEFROM, ValueFrom);

        if(speedFrom.equals("kmph")&&speedTo.equals("mph"))
        {
            double result = this.KmhToMph();
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
        }
        else if(speedFrom.equals("mph")&&speedTo.equals("kmph"))
        {
            double result = this.MphToKmh();
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
        }
        else
        {
            double result = Double.parseDouble(etEnterValue.getText().toString());
            intent.putExtra(ResultActivity.KEY_VALUETO, result);

        }
        this.startActivity(intent);

    }
}
