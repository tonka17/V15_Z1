package hr.ferit.apoljak.v15_z1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends Activity {

    TextView tvConvertedFrom;
    TextView tvConvertedTo;
    TextView tvValueFrom;
    TextView tvValueTo;

    public static final String KEY_CONVERTEDFROM = "temperature1";
    public static final String KEY_CONVERTEDTO = "temperature2";
    public static final String KEY_VALUEFROM = "value1";
    public static final String KEY_VALUETO = "value2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        tvConvertedFrom=(TextView) findViewById(R.id.tvConvertedFrom);
        tvConvertedTo=(TextView) findViewById(R.id.tvConvertedTo);
        tvValueFrom=(TextView) findViewById(R.id.tvValueFrom);
        tvValueTo=(TextView) findViewById(R.id.tvValueTo);

        this.handleStartingIntent(this.getIntent());

    }

    private void handleStartingIntent(Intent intent)
    {
        if(intent!=null)
        {
            if(intent.hasExtra(KEY_CONVERTEDFROM))
            {
                String ConvertedFrom=intent.getStringExtra(KEY_CONVERTEDFROM);
                tvConvertedFrom.setText(ConvertedFrom);
            }
            if(intent.hasExtra(KEY_CONVERTEDTO))
            {
                String ConvertedTo=intent.getStringExtra(KEY_CONVERTEDTO);
                tvConvertedTo.setText(ConvertedTo);
            }
            if(intent.hasExtra(KEY_VALUEFROM))
            {
                double ValueFrom=intent.getDoubleExtra(KEY_VALUEFROM, 0);
                tvValueFrom.setText(String.valueOf(ValueFrom));
            }
            if(intent.hasExtra(KEY_VALUETO))
            {
                double ValueTo=intent.getDoubleExtra(KEY_VALUETO, 0);
                tvValueTo.setText(String.valueOf(ValueTo));
            }

        }
    }
}
