package hr.ferit.apoljak.v15_z1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class VolumeActivity extends Activity implements View.OnClickListener {

    ArrayAdapter<CharSequence> adapterVolume1;
    ArrayAdapter<CharSequence> adapterVolume2;
    EditText etEnterValue;
    Button bConvert;
    String volumeFrom="";
    String volumeTo="";
    Spinner spinnerVolume1;
    Spinner spinnerVolume2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume);

        etEnterValue = (EditText) findViewById(R.id.etEnterValue);
        bConvert = (Button) findViewById(R.id.bConvert);
        bConvert.setOnClickListener(this);
        spinnerVolume1 = (Spinner)findViewById(R.id.spinnerVolume1);
        spinnerVolume2 = (Spinner)findViewById(R.id.spinnerVolume2);
        adapterVolume1 = ArrayAdapter.createFromResource(this, R.array.spinnerVolumeArray1, android.R.layout.simple_spinner_item);
        adapterVolume2 = ArrayAdapter.createFromResource(this, R.array.spinnerVolumeArray2, android.R.layout.simple_spinner_item);
        adapterVolume1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterVolume2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerVolume1.setAdapter(adapterVolume1);
        spinnerVolume2.setAdapter(adapterVolume2);

        spinnerVolume1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                volumeFrom = (String)parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerVolume2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view2, int position, long id) {
                volumeTo=(String)parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    public double LiterToGallon()
    {
        double valueLtG = Double.parseDouble(etEnterValue.getText().toString());
        return valueLtG / 3.78541;
    }

    public double GallonToLiter()
    {
        double valueGtL = Double.parseDouble(etEnterValue.getText().toString());
        return valueGtL *3.78541;
    }

    @Override
    public void onClick(View v)
    {
        Intent intent = new Intent();
        intent.setClass(getBaseContext(), ResultActivity.class);

        String VolumeFrom = volumeFrom;
        intent.putExtra(ResultActivity.KEY_CONVERTEDFROM, VolumeFrom);

        String VolumeTo = volumeTo;
        intent.putExtra(ResultActivity.KEY_CONVERTEDTO, VolumeTo);

        double ValueFrom = Double.parseDouble(etEnterValue.getText().toString());
        intent.putExtra(ResultActivity.KEY_VALUEFROM, ValueFrom);

        if(VolumeFrom.equals("liter")&&VolumeTo.equals("gallon"))
        {
            double result = this.LiterToGallon();
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
        }
        else if(VolumeFrom.equals("gallon")&&VolumeTo.equals("liter"))
        {
            double result = this.GallonToLiter();
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
        }
        else
        {
            double result = Double.parseDouble(etEnterValue.getText().toString());
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
        }
        this.startActivity(intent);

    }
}
