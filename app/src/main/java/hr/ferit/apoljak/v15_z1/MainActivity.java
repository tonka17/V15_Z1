package hr.ferit.apoljak.v15_z1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity implements View.OnClickListener {

    ImageView iTemperature;
    ImageView iLength;
    ImageView iVolume;
    ImageView iSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initializeUI();
    }

    private void initializeUI()
    {
        this.iTemperature=(ImageView) findViewById(R.id.iTemperature);
        this.iTemperature.setOnClickListener(this);
        this.iLength=(ImageView)findViewById(R.id.iLength);
        this.iLength.setOnClickListener(this);
        this.iVolume=(ImageView)findViewById(R.id.iVolume);
        this.iVolume.setOnClickListener(this);
        this.iSpeed=(ImageView)findViewById(R.id.iSpeed);
        this.iSpeed.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
       Intent intent = new Intent();
       int id = v.getId();
       switch(id)
       {
           case R.id.iTemperature:
               intent.setClass(getBaseContext(), TemperatureActivity.class);
               startActivity(intent);
               break;
           case R.id.iLength:
               intent.setClass(getBaseContext(), DistanceActivity.class);
               startActivity(intent);
               break;
           case R.id.iVolume:
               intent.setClass(getBaseContext(), VolumeActivity.class);
               startActivity(intent);
               break;
           case R.id.iSpeed:
               intent.setClass(getBaseContext(), SpeedActivity.class);
               startActivity(intent);
               break;

       }
    }
}
