package hr.ferit.apoljak.v15_z1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class DistanceActivity extends Activity implements View.OnClickListener {

    ArrayAdapter<CharSequence> adapterLength1;
    ArrayAdapter<CharSequence> adapterLength2;
    EditText etEnterValue;
    Button bConvert;
    String LengthFrom="";
    String LengthTo="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);

        etEnterValue = (EditText)findViewById(R.id.etEnterValue);
        bConvert = (Button)findViewById(R.id.bConvert);
        bConvert.setOnClickListener(this);

        Spinner spinnerLength1 = (Spinner)findViewById(R.id.spinnerLength1);
        adapterLength1 = ArrayAdapter.createFromResource(this, R.array.spinnerLength1, android.R.layout.simple_spinner_item);
        adapterLength1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLength1.setAdapter(adapterLength1);

        Spinner spinnerLength2 = (Spinner)findViewById(R.id.spinnerLength2);
        adapterLength2 = ArrayAdapter.createFromResource(this, R.array.spinnerLength2, android.R.layout.simple_spinner_item);
        adapterLength2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLength2.setAdapter(adapterLength2);

        spinnerLength1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                LengthFrom = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerLength2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view2, int position, long id) {
                LengthTo = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public double InchesToMeters()
    {
        String valueItMS = this.etEnterValue.getText().toString();
        double valueItM = Double.parseDouble(valueItMS);
        return valueItM * 0.0254;
    }
    public double MetersToInches()
    {
        String valueMtIS = this.etEnterValue.getText().toString();
        double valueMtI = Double.parseDouble(valueMtIS);
        return valueMtI / 0.0254;
    }


    @Override
    public void onClick(View v)
    {
        Intent intent = new Intent();
        intent.setClass(this, ResultActivity.class);
        String ConvertedFrom = LengthFrom;
        intent.putExtra(ResultActivity.KEY_CONVERTEDFROM, ConvertedFrom);

        String ConvertedTo = LengthTo;
        intent.putExtra(ResultActivity.KEY_CONVERTEDTO, ConvertedTo);

        double ValueFrom = Double.parseDouble(etEnterValue.getText().toString());
        intent.putExtra(ResultActivity.KEY_VALUEFROM, ValueFrom);

        if(LengthFrom.equals("inches")&&LengthTo.equals("meters"))
        {
            double result = this.InchesToMeters();
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
        }
        else if(LengthFrom.equals("meters")&&LengthTo.equals("inches"))
        {
            double result = this.MetersToInches();
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
        }
        else
        {
            double result = Double.parseDouble(etEnterValue.getText().toString());
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
        }
        this.startActivity(intent);
    }
}
