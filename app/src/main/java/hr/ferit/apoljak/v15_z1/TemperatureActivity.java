package hr.ferit.apoljak.v15_z1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class TemperatureActivity extends Activity implements View.OnClickListener {


    ArrayAdapter<CharSequence> adapterTemperature1;
    ArrayAdapter<CharSequence> adapterTemperature2;
    EditText etEnterValue;
    Button bConvert;
    String temperatureFrom="";
    String temperatureTo="";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);


        etEnterValue = (EditText) findViewById(R.id.etEnterValue);

        bConvert = (Button) findViewById(R.id.bConvert);
        bConvert.setOnClickListener(this);

        Spinner spinnerTemperature1 = (Spinner) findViewById(R.id.spinnerTemperature1);
        adapterTemperature1 = ArrayAdapter.createFromResource(this, R.array.spinnerTemperatureArray1, android.R.layout.simple_spinner_item);
        adapterTemperature1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTemperature1.setAdapter(adapterTemperature1);

        Spinner spinnerTemperature2 = (Spinner) findViewById(R.id.spinnerTemperature2);
        adapterTemperature2 = ArrayAdapter.createFromResource(this, R.array.spinnerTemperatureArray2, android.R.layout.simple_spinner_item);
        adapterTemperature2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTemperature2.setAdapter(adapterTemperature2);

        spinnerTemperature1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                temperatureFrom = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });


        spinnerTemperature2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view2, int position, long id)
            {
            temperatureTo = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });
    }
       public double CelsiusToFahren()
        {
            String valueCtF = this.etEnterValue.getText().toString();
            double valueCtFd = Double.parseDouble(valueCtF);
            return valueCtFd*9/5 + 32;

        }

    public double FahrenToCelsius()
    {
        String valueFtC = this.etEnterValue.getText().toString();
       double valueFtCd = Double.parseDouble(valueFtC);
      return (valueFtCd-32) / 1.8;
   }


   public void onClick(View v)
    {

        Intent intent = new Intent();
        intent.setClass(this, ResultActivity.class);
        String ConvertedFrom = temperatureFrom;
        intent.putExtra(ResultActivity.KEY_CONVERTEDFROM, ConvertedFrom);


        String ConvertedTo = temperatureTo;
        intent.putExtra(ResultActivity.KEY_CONVERTEDTO, ConvertedTo);

        String ValueFromS = this.etEnterValue.getText().toString();
        double ValueFrom = Double.parseDouble(ValueFromS);
        intent.putExtra(ResultActivity.KEY_VALUEFROM, ValueFrom);

        if(temperatureFrom.equals("Celsius")&&temperatureTo.equals("Fahrenheit"))
        {
            double result = this.CelsiusToFahren();
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
            //     this.tvTemperatureConversion.setText(String.valueOf(result));
        }
        else if(temperatureFrom.equals("Fahrenheit")&&temperatureTo.equals("Celsius"))
        {
            double result = this.FahrenToCelsius();
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
            //    this.tvTemperatureConversion.setText(String.valueOf(result));
        }
        else
        {
            String sResult = this.etEnterValue.getText().toString();
            double result = Double.parseDouble(sResult);
            intent.putExtra(ResultActivity.KEY_VALUETO, result);
            //   this.tvTemperatureConversion.setText(String.valueOf(result));
        }

        this.startActivity(intent);

    }

}
